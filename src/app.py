import pandas as pd
import streamlit as st
from pycaret import *
from preprocess_features import FeaturePreprocessor
from predict import *
from save_data import *
from train import *
from config import Config
import streamlit_authenticator as stauth


names = ['churn@jumb0ta!l']
usernames = ['churn@jumb0ta!l']
passwords = ['Jumb0@tail09']

hashed_passwords = stauth.Hasher(passwords).generate()

authenticator = stauth.Authenticate(names, usernames, hashed_passwords, 'cookie_name', 'signature_key', cookie_expiry_days=1)

name, authentication_status, username = authenticator.login('Login', 'sidebar')

pred_df = pd.read_csv(str(Config.FINAL_PREDICT_DATA_PATH) + "/predict_dataset.csv")

prediction_data = pred_df.to_csv().encode('utf-8')
def run_app():
    st.title("Churn Prediction")

    # st.markdown(
    #     "Welcome to this simple web application that classifies shops. The shops are classified into two different classes namely: open and closed."
    # )
    ui = st.sidebar.radio(
        "This is the area we can use ",
        ("Train", "Predict", "Download Data", "Model Comparisons", "Confusion Matrix", "SHAP Values", "Dashboard"),
    )
    if ui == "Train":
        st.subheader("Train the model")
        st.markdown("This section is used to train the model.")
        if st.button("Start Training"):
            with st.spinner("Training started..."):
                ModelTrainer().train_model()
                st.success("Training completed")
    if ui == "Predict":
        st.subheader("Predict the churn")
        st.markdown("This section is used to predict the churn.")
        if st.button("Start Prediction"):
            with st.spinner("Prediction started..."):
                ModelPredictor().prediction_model()
                st.success("Prediction completed")
    if ui == "Download Data":
        st.subheader("Download Data")
        st.markdown("This section is used to fetch data from the database.")
        if st.download_button(label="Download data as CSV",data=prediction_data,file_name='predict_df.csv',mime='text/csv'):
            st.success("data downloaded")


    if ui == "Model Comparisons":
        st.subheader("Model Comparisons")
        st.markdown("This section is used to compare the models.")
        df = pd.read_csv(str(Config.TRAIN_MODEL_OUTPUT_PATH) + "/model_comparison.csv")
        st.dataframe(df.style.highlight_max(axis=0))
        # st.success("Model comparisons loaded")
    if ui == "Confusion Matrix":
        st.subheader("Confusion Matrix")
        st.markdown("This section is used to view the confusion matrix.")
        st.write("0 : No churn | 1 : Churn")
        st.write("X Axis : Predicted Class | Y Axis : Actual Class")
        st.image("Confusion Matrix.png", caption="Confusion Matrix")
        # st.success("Model results loaded")
    if ui == "SHAP Values":
        st.subheader("SHAP Values")
        st.markdown("This section is used to view the SHAP values.")
        # model = load_model("final_model_pipeline")
        # interpret_model(model, display_format="streamlit")
        st.image("SHAP summary.png", caption="SHAP Values")
        # st.success("Model results loaded")



if __name__ == "__main__":
    if authentication_status:
        run_app()
    elif authentication_status == False:
        st.error("Username / password is incorrect")
    elif authentication_status == None:
        st.warning("Please enter your username and password")

