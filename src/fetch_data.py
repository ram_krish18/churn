import json
from read_json import JSON_Reader
import requests
from config import Config
import psycopg2 as psycopg2
import pandas as pd
import numpy as np
import datetime
from datetime import date
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score, f1_score, precision_score, recall_score
from sklearn.metrics import (
    confusion_matrix,
    roc_auc_score,
    roc_curve,
    classification_report,
    precision_recall_curve,
)
from sklearn.model_selection import (
    train_test_split,
    cross_val_score,
    StratifiedKFold,
    GridSearchCV,
    RandomizedSearchCV,
)
from collections import Counter
import time
from psycopg2.extras import NamedTupleCursor
import sqlalchemy as SA
from sqlalchemy.exc import SQLAlchemyError

# from Query_for_data import *


class DataFetcher:
    def __init__(self):
        self.json_reader_object = JSON_Reader()
        self.secrets_dict = self.json_reader_object.read_json_file(
            file_path=Config.SECRET_FILE_PATH
        )

    def fetch_data(self, query):

        output = []
        redshiftdata = {}
        list1 = []

        try:
            con = psycopg2.connect(
                host=self.secrets_dict["red_host"],
                user=self.secrets_dict["name"],
                password=self.secrets_dict["password"],
                database=self.secrets_dict["db_name"],
                port=self.secrets_dict["port"],
            )
            cur = con.cursor(cursor_factory=NamedTupleCursor)
            cur.execute(query)
            output = cur.fetchall()
            output = pd.DataFrame(output)
            return output

        except psycopg2.Error as e:

            print("Error Occurred -> ", e)
            return []

    def create_table(self, query):

        output = []
        redshiftdata = {}
        list1 = []

        try:
            con = psycopg2.connect(
                host=self.secrets_dict["red_host"],
                user=self.secrets_dict["name"],
                password=self.secrets_dict["password"],
                database=self.secrets_dict["db_name"],
                port=self.secrets_dict["port"],
            )
            cur = con.cursor()
            cur.execute(query)
            con.commit()

        except psycopg2.Error as e:

            print("Error Occurred -> ", e)

    def create_table_sql_alchemy_final(self):

        output = []
        redshiftdata = {}
        list1 = []
        pred_churn_data = pd.read_csv(str(Config.PREDICT_MODEL_OUTPUT_PATH) + "/predict_data_labels.csv")
        pred_churn_data['model_run_date'] = '2022-03-01'
        #pred_upload_data = pred_churn_data[['businessid','model_run_date','Label','Score_1']]

        url = "{d}+{driver}://{u}:{p}@{h}:{port}/{db}". \
            format(d="redshift",
                   driver='psycopg2',
                   u=self.secrets_dict["name"],
                   p=self.secrets_dict["password"],
                   h=self.secrets_dict["red_host"],
                   port=self.secrets_dict["port"],
                   db=self.secrets_dict["db_name"],)

        try:
            engine = SA.create_engine(url)
            cnn = engine.connect()
            pred_churn_data.to_sql(
                'predicted_churn',
                con=cnn,
                index=False,
                if_exists='append',
                method = 'multi',
                chunksize = 1000
            )
        except SQLAlchemyError as e:

            print("Error Occurred -> ", e)


if __name__ == "__main__":
    # read_secrets_file(Config.SECRET_FILE_PATH)
    # fetch_data(queryString_input)
    # df = DataFetcher(queryString_input)
    # df.read_secrets_file(Config.SECRET_FILE_PATH)
    # df.fetch_data(queryString_input)
    dfo = DataFetcher()
    dfo.fetch_data()
