from query import Queries
from config import Config
from fetch_data import DataFetcher
import pandas as pd
import numpy as np
from datetime import date
from datetime import time
from datetime import datetime
from datetime import timedelta
from save_data import *
from transform_data import *
from preprocess_features import *
from pycaret.classification import *
from train import *
from predict import *





if __name__ == "__main__":
    save_data = SaveData()
    save_data.save_data_train()
    save_data.save_data_pred()
    data_transform = DataTransformer()
    data_transform.transform_train_data()
    data_transform.merge_transformed_train_data()
    data_transform.transform_predict_data()
    data_transform.merge_transformed_predict_data()
    model_trainer = ModelTrainer()
    feature_preprocessor = FeaturePreprocessor()
    model_trainer.train_model(feature_preprocessor.preprocess_features())
    model_predictor = ModelPredictor()
    model_predictor.prediction_model(feature_preprocessor.preprocess_features_predict())
