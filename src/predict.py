from pycaret.classification import *
from preprocess_features import *
from config import Config
from shapash.explainer.smart_explainer import SmartExplainer



class ModelPredictor:
    def __init__(self):
        pass

    def prediction_model(self, final_pred_dataset=FeaturePreprocessor().preprocess_features_predict()):

        model_prediction = load_model("final_model_pipeline")
        prediction_data_score = predict_model(
            model_prediction, data=final_pred_dataset, raw_score=True
        )
        Prediction_Score_label = prediction_data_score[["Label", "Score_1"]]

        pred_df = pd.read_csv(str(Config.FINAL_PREDICT_DATA_PATH) + "/predict_dataset.csv")
        pred_data_labels = pd.concat([pred_df, Prediction_Score_label], axis=1)


        pred_data_labels_final = pd.merge(pred_data_labels, city_data, how='left', left_on=['businessid'],
                                           right_on=['businessid'])

        pred_data_labels_final.to_csv(
            str(Config.PREDICT_MODEL_OUTPUT_PATH) + "/predict_data_labels.csv", index=None
        )



if __name__ == "__main__":
    model_predictor = ModelPredictor()
    feature_preprocessor = FeaturePreprocessor()
    model_predictor.prediction_model(feature_preprocessor.preprocess_features_predict())
