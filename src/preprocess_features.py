from config import Config
import pandas as pd

class FeaturePreprocessor:
    def __init__(self):
        pass

    def preprocess_features(self):
        train_df = pd.read_csv(str(Config.FINAL_TRAIN_DATA_PATH)+"/train_dataset.csv")
        final_dataset = train_df.iloc[:, 2:]
        final_dataset = pd.get_dummies(data=final_dataset, columns=['current_state', 'status'])
        return final_dataset

    def preprocess_features_predict(self):
        pred_df = pd.read_csv(str(Config.FINAL_PREDICT_DATA_PATH)+"/predict_dataset.csv")
        final_pred_dataset = pred_df.iloc[:, 1:]
        final_pred_dataset = pd.get_dummies(data=final_pred_dataset, columns=['current_state', 'status'])
        return final_pred_dataset


if __name__ == "__main__":
    print(FeaturePreprocessor().preprocess_features().columns)
    print(FeaturePreprocessor().preprocess_features_predict().columns)

