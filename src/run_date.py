class date_announcer:
    def __init__(self):
        pass

    def run_date_time_global(self):
        model_run_date_time = datetime.datetime.now()
        return(model_run_date_time)


    def run_date_global(self):
        model_run_date = datetime.datetime.now().strftime("%Y-%m-%d")
        return(model_run_date)