from query import Queries
from config import Config
from fetch_data import DataFetcher
import pandas as pd


class SaveData:
    def __init__(self):
        self.queries_dict = Queries().get_train_queries()
        self.pred_queries_dict = Queries().get_predict_queries()
        self.biz_info_dict = Queries().get_info_queries()
        self.pred_table_creator = Queries().table_creation_query_pred()
        self.pred_feat_table_creator = Queries().table_creation_query_feat()
        self.train_table_creator = Queries().table_creation_query_train()


    def save_data_train(self):
        for query_name, query in self.queries_dict.items():
            data_fetcher = DataFetcher()
            data_fetcher.fetch_data(query).to_csv(
                str(Config.RAW_TRAIN_DATA_PATH) + "/" + query_name + ".csv", index=False
            )
            print(query_name + ': output is ready')

    def save_data_pred(self):
        for query_name, query in self.pred_queries_dict.items():
            data_fetcher = DataFetcher()
            data_fetcher.fetch_data(query).to_csv(
                str(Config.RAW_PREDICT_DATA_PATH) + "/" + query_name + "_predict.csv", index=False
            )
            print(query_name + ': output is ready')

    def save_business_info(self):
        for query_name, query in self.biz_info_dict.items():
            data_fetcher = DataFetcher()
            data_fetcher.fetch_data(query).to_csv(
                str(Config.RAW_BIZINFO_DATA_PATH) + "/" + query_name + ".csv", index=False
            )
            print(query_name + ': output is ready')


    def insert_data_table_final(self):
        data_fetcher = DataFetcher()
        data_fetcher.create_table_sql_alchemy_final()
        print('churn_prediction : output table updated')

if __name__ == "__main__":
    save_data = SaveData()
    #save_data.save_data_train()
    #save_data.save_data_pred()
    #save_data.save_business_info()
    #save_data.create_table_struct_pred()
    #save_data.create_table_struct_feat()
    #save_data.create_table_struct_train()
    save_data.insert_data_table_final()
