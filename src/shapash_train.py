from pycaret.classification import *
from shapash.explainer.smart_explainer import SmartExplainer
from config import Config
from preprocess_features import FeaturePreprocessor
import pandas as pd


class ModelTrainershap:
    def __init__(self):
        pass

    def train_model_shap(self, final_dataset=FeaturePreprocessor().preprocess_features(),final_pred_dataset=FeaturePreprocessor().preprocess_features_predict()):
        s = setup(data=final_dataset, target="churn_ind", session_id=123, silent=True)
        best_model = compare_models()

        model_best = create_model(best_model)
        tuned_model = tune_model(model_best, choose_better=True)
        final_model = finalize_model(tuned_model)

        prep_pipe = get_config('prep_pipe')
        pred_shap_data = prep_pipe.transform(final_pred_dataset)

        pred_shap_data.to_csv(
            str(Config.FINAL_PREDICT_DATA_PATH) + "/pred_shapash_data.csv", index=None
        )

        prediction_data = pd.read_csv(str(Config.PREDICT_MODEL_OUTPUT_PATH) + "/predict_data_labels.csv")
        predictions_label_final = prediction_data[['Label']]

        xplclf = SmartExplainer()
        xplclf.compile(x=pred_shap_data, model=final_model, y_pred=predictions_label_final)
        summary_df = xplclf.to_pandas(proba=True, max_contrib=5, positive=True)
        pred_df_bzid = prediction_data[['businessid','current_state']]


        summary_df_final = pd.concat([summary_df, pred_df_bzid], axis=1)

        print(summary_df_final.shape)

        city_data = pd.read_csv(str(Config.RAW_BIZINFO_DATA_PATH) + "/business_info.csv")
        city_data = city_data.drop_duplicates(subset=["businessid"])

        summary_df_final_with_city = pd.merge(city_data, summary_df_final, how='left', left_on=['businessid'], right_on=['businessid'])

        print(summary_df_final_with_city.shape)

        summary_df_final_with_city.to_csv(
            str(Config.PREDICT_MODEL_OUTPUT_PATH) + "/feature_importance.csv", index=None
        )

if __name__ == "__main__":
    model_trainer_shap = ModelTrainershap()
    feature_preprocessor = FeaturePreprocessor()
    model_trainer_shap.train_model_shap(feature_preprocessor.preprocess_features(),feature_preprocessor.preprocess_features_predict())